 
 ## awaitUntil(opts|predicate)
 
   Runs `worker` after every `interval` until `predicate` function returns `truthy` or run out of `maxRetries`. If `worker` is undefined, then `predicate` should be used to check some state to determine whether to continue.

    - `OPTIONS`:
      * @param **`predicate`** _`{Function}`_ - test to see if worker was successful, must return truthy to indicate success.
      * @param **`worker?`** _`{Function|undefined}`_ - _(maybe async)_ function to run and do something that returns a result to be analysed by predicate function. Invoked with (prevResult, iteration).
      * @param **`maxRetries?`** _`{Number}`_ - number of retries before we quit and reject. Use Infinity to wait forever. _Default 600_.
      * @param **`interval?`** _`{Number}`_ - interval between retries. _Default 100_.
      * @param **`waitFirst?`** _`{Boolean}`_ - wait 1 interval first before executing worker for the first time. _Default false._
      * @return _`{Promise<*>}`_ - _resolves/rejects_ to the actual result of the worker function or result of predicate function.
    
   ```js
 // this will try to get result within 1 sec for 30 times by running doSomeAsyncWork function at each try
 let res = await awaitUntil({worker : { 
    worker: (prevResult, iteration)=>{/*do some work and return result to be eval by predicate*/}, 
    predicate: (result)=>{/*evaluate/analyse result and return true/false to break/continue waiting*/},
    maxRetries: 30, 
    interval: 1000
}}).catch(result=>{/*do something when we have failed*/});
```
or to use defaults:
   ```js
 // this will try to get result within 1 sec for 30 times by running doSomeAsyncWork function at each try
 let res = await awaitUntil((result)=>{/*evaluate/analyse state return true/false to break/continue waiting*/}).catch(result=>{/*do something when we have failed*/});
```

`.then` or all other promise methods can be used since this is a promise.

## waitFor() WIP* has a few issues, on certain objects, but works

 Fast, non-polling wait for mutation or existence of an object's property
   * @param object {Object} - Object to check property on
   * @param property {String} - property to check for
 
 useful when you don't want to do something before a property, function etc is available
 
 ```js
waitFor(app, 'someProperty').then(r=>{
  // something with the prop
  app.someProperty()
})
```


## Author

Emmanuel Mahuni 

MIT
