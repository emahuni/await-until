export interface AwaitUntilParams {
  predicate: Function;
  worker?: Function | undefined;
  maxRetries?: number;
  interval?: number;
  waitFirst?: boolean;
  debug?: boolean;
}

/**
 * Wait for an interval of time and repeat execution of a worker so many times (repeat) until predicate function receives truthy result from worker.
 * - default total timeout is 60 seconds
 * - this generally runs worker after every interval until predicate function returns truthy or run out of retries
 * @param options.worker  - (maybe async) function to run and do something that returns a result to be analysed by predicate
 * @param options.predicate  - test to see if worker was successful, must return truthy to indicate success.
 * @param options.maxRetries - default to 600. number of retries before we quit, use Infinity to indicate such. alias retries
 * @param options.interval  - defaults to 100. interval between retries
 * @param options.waitFirst  - wait 1 interval first before executing worker for the first time. alias wait1st
 * @param options.debug
 * @return {Promise<*>} - the actual result of the worker function
 */
export default function awaitUntil (options: AwaitUntilParams) {
  if (typeof options === 'function') options = { predicate: options };
  let {
    predicate, worker = undefined,
    maxRetries = 600, interval = 100, waitFirst = false,
    debug = false,
  } = options ?? {};
  
  /** backwards compatability */
  // @ts-ignore
  if (options.retries !== undefined && options.maxRetries === undefined) maxRetries = options.retries;
  // @ts-ignore
  if (options.wait1st !== undefined && options.waitFirst === undefined) waitFirst = options.wait1st;
  // @ts-ignore
  if (options.predicateFn !== undefined && options.predicate === undefined) predicate = options.predicateFn;
  
  if (!predicate) throw new Error(`[index/awaitUntil()]-31: Missing required param predicate!`);
  
  let result;
  
  return new Promise(async (resolve: Function, reject: Function) => {
    for (let retries = 0; retries < maxRetries; retries++) {
      // don't wait on the first try, just go straight to await promise
      if (!!worker) {
        if (retries === 0 && !waitFirst) {
          result = await worker(result, retries);
        } else {
          result = await new Promise<any>(reso => setTimeout(async () => reso(await worker(result, retries)), interval));
        }
      } else {
        await new Promise<void>(reso => setTimeout(async () => reso(), interval));
      }
      
      // make sure we have all the information before we continue
      let res;
      if (!!(res = predicate(result))) {
        if (!worker) result = res;
        if (debug) console.debug(`[await-until]-30: awaitUntil() - resolution result: %o`, result);
        resolve(result);
        return;
      }
      
      // console.debug(`utilities: waitUntil() - retries #: %o...`, retries);
    }
    
    if (debug) console.debug(`[await-until]-30: awaitUntil() - error - predicateFn didn't pass for given time...%o`, result);
    reject(`predicate() Function didn't pass for given time intervals and retries... ` + result);
  });
}


/**
 * Fast, non-polling wait for mutation or existence of an object's property
 * @param object - Object to check property on
 * @param property - property to check for
 */
export function waitFor (object: object, property: string): Promise<any> {
  return new Promise<any>(resolve => {
    const initVal = object[property];
    
    // redefine the property with our trap set for changes
    Object.defineProperty(object, property, {
      configurable: true, value: initVal,
      // set a trap for any assignment to prop
      set (v) {
        // finally set the new value
        Object.defineProperty(object, property, {
          configurable: true, enumerable: true, writable: true, value: v,
        });
        
        resolve(v);
      },
    });
  });
}

